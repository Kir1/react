const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();
app.use(express.static(path.join(__dirname, 'build')));


var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'kasper'
});
connection.connect();
connection.query('SELECT * FROM days', function (error, results, fields) {
    if (error) throw error;
    console.log(results);
    //console.log('The solution is: ', results[0].solution);
});
connection.end();

app.post('/posts', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    return res.send(JSON.stringify({ a: 514 }));
});

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(process.env.PORT || 8080);
