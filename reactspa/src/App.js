import React from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import {connect} from 'react-redux';
import {changePage} from './modules/user/UserActions';
import UsersList from './modules/user/containers/UsersList';
import UserPage from './modules/user/containers/UserPage';
import Index from './modules/user/components/Index';
import {Navbar, Nav} from 'react-bootstrap';

// App.js
export class App extends React.Component {

    goToPage = (e, page) => {
        this.props.dispatch(changePage(page))
    }

    render() {
        return (
            <Router>
                <div className="mb-5">
                    <div className="bg-light mb-3">
                        <div className="container">
                            <Navbar bg="light" expand="lg" className="col">
                                <Navbar.Brand href="#home">NEORIX</Navbar.Brand>
                                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                                <Navbar.Collapse id="basic-navbar-nav">
                                    <Nav className="mr-auto">
                                        <Nav>
                                            <Link className="nav-link" onClick={(e) => this.goToPage(e, 'Главная')}
                                                  to="/">Главная</Link>
                                        </Nav>
                                        <Nav>
                                            <Link className="nav-link"
                                                  onClick={(e) => this.goToPage(e, 'Список сотрудников')} to="/users/">Список
                                                сотрудников</Link>
                                        </Nav>
                                    </Nav>
                                </Navbar.Collapse>
                            </Navbar>
                        </div>
                    </div>

                    <div className="container">
                        <Route path="/" exact component={Index}/>

                        <Route path="/users/" exact component={UsersList}/>

                        <Route path="/users/:id" exact component={UserPage}/>
                    </div>
                </div>
            </Router>
        );
    }
}

function stateUserList(state) {
    return {
        usersReducer: state.usersReducerContainer
    };
}

export default connect(stateUserList)(App);