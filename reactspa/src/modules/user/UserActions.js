export const changePage = (page) => ({
    type: 'CHANGE_PAGE',
    page: page
});

export const professionFilterUsers = (value) => ({
    type: 'PROFESSION_FILTER_USERS',
    profession: value
});

export const archiveStatusFilterUsers = (value) => ({
    type: 'ARCHIVE_STATUS_FILTER_USERS',
    status: value
});

export const sortUsers = (value) => ({
    type: 'SORT_USERS',
    sort_by: value
});

export const formChangeProfession = (value) => ({
    type: 'FORM_CHANGE_PROFESSION',
    profession: value
});

export const formChangeBirthday = (value) => ({
    type: 'FORM_CHANGE_BIRTHDAY',
    birthday: value
});

export const formChangeArchive = (value) => ({
    type: 'FORM_CHANGE_ARCHIVE',
    status: value
});

export const formChangeName = (value) => ({
    type: 'FORM_CHANGE_NAME',
    name: value
});

export const formChangePhone = (value) => ({
    type: 'FORM_CHANGE_PHONE',
    phone: value
});

export const createUser = (user) => ({
    type: 'CREATE_USER',
    user_form: user
});

export const editUser = (user) => ({
    type: 'EDIT_USER',
    user_form: user
});

export const formUserError = (errors) => ({
    type: 'FORM_USER_ERROR',
    errors: errors
});