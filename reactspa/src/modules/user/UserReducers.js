import {USERS_PROFESSIONS, USERS_SORT_FILTERS, USERS_INI, ARCHIVE_STATUSES} from './UserMain';
import moment from 'moment';

let users_out_archive = [];
for (var key in USERS_INI) {
    if (!USERS_INI[key].isArchive) users_out_archive.push(Object.assign({}, USERS_INI[key]));
}

let iniUsersReducer = {
    users: USERS_INI,
    filtered_users: users_out_archive,
    profession: USERS_PROFESSIONS.all,
    sort_by: USERS_SORT_FILTERS.name,
    in_archive: false
};

function sortByName(user1, user2) {
    if (user1.name < user2.name)
        return -1;
    if (user1.name > user2.name)
        return 1;
    return 0;
}

function sortByBirthdate(user1, user2) {
    let birthday1 = moment(user1.birthday, 'DD.MM.YYYY').unix(),
        birthday2 = moment(user2.birthday, 'DD.MM.YYYY').unix();
    if (birthday1 < birthday2)
        return -1;
    if (birthday1 > birthday2)
        return 1;
    return 0;
}

export const usersReducer = (state = iniUsersReducer, action) => {
    if (
        action.type == 'PROFESSION_FILTER_USERS'
        || action.type == 'ARCHIVE_STATUS_FILTER_USERS'
        || action.type == 'SORT_USERS'
        || action.type == 'CREATE_USER'
        || action.type == 'EDIT_USER'
    ) {
        let return_reducer = Object.assign({}, state),
            list_users = [],
            all_users = return_reducer.users,
            in_archive = return_reducer.in_archive,
            profession = return_reducer.profession,
            check_profession = true,
            get_user_all_professions,
            sort_by = return_reducer.sort_by,
            birthday_format;

        if (action.type == 'CREATE_USER') {
            birthday_format = moment(action.user_form.birthday).format('DD.MM.YYYY');
            let new_user = {
                id: action.user_form.id,
                name: action.user_form.name,
                isArchive: (action.user_form.in_archive.value == 'yes') ? true : false,
                role: action.user_form.profession.value,
                phone: action.user_form.phone,
                birthday: birthday_format
            };

            console.log('Новый работник успешно добавлен в систему', new_user);

            all_users.push(new_user);
        }

        if (action.type == 'EDIT_USER') {
            birthday_format = moment(action.user_form.birthday).format('DD.MM.YYYY');
            let user = {
                id: action.user_form.id,
                name: action.user_form.name,
                isArchive: (action.user_form.in_archive.value == 'yes') ? true : false,
                role: action.user_form.profession.value,
                phone: action.user_form.phone,
                birthday: birthday_format
            };

            console.log('Работник отредактирован', user);

            for (var key in all_users) {
                if (all_users[key].id == user.id) {
                    all_users[key] = user;
                    break;
                }
            }
        }

        if (action.type == 'ARCHIVE_STATUS_FILTER_USERS') {
            in_archive = action.status;
            if (in_archive) console.log('Работники из архива');
            else console.log('Работники на ставке');
        }

        if (action.type == 'PROFESSION_FILTER_USERS') {
            profession = action.profession;
            if (profession.value == 'all') console.log('Работники всех профессий');
            else console.log('Работники с профессиией "' + action.profession.label + '"');
        }

        get_user_all_professions = (profession.value == 'all');

        for (var key in all_users) {
            if (get_user_all_professions) check_profession = true;
            else {
                if (action.type == 'PROFESSION_FILTER_USERS') {
                    check_profession = (all_users[key].role == action.profession.value);
                } else {
                    check_profession = (all_users[key].role == state.profession.value);
                }
            }

            if (check_profession && all_users[key].isArchive == in_archive) list_users.push(all_users[key])
        }

        if (action.type == 'SORT_USERS') {
            sort_by = action.sort_by;
            console.log('Работники остортированы по полю "' + action.sort_by.label + '"');
        }

        if (sort_by.value == 'birthdate') list_users.sort(sortByBirthdate);
        else if (sort_by.value == 'name') list_users.sort(sortByName);

        if (action.type != 'EDIT_USER') console.log(list_users);

        return_reducer.filtered_users = list_users;
        return_reducer.profession = profession;
        return_reducer.in_archive = in_archive;
        return_reducer.sort_by = sort_by;

        return return_reducer;
    }

    switch (action.type) {
        case 'CHANGE_PAGE':
            console.log('Переход на страницу - "' + action.page + '"');
            return state
        case 'FORM_CHANGE_NAME':
            console.log('Значение поля "Имя" в форме работника - "' + action.name + '"');
            return state
        case 'FORM_CHANGE_PHONE':
            console.log('Значение поля "Номер телефона" в форме работника - "' + action.phone + '"');
            return state
        case 'FORM_CHANGE_PROFESSION':
            console.log('Значение поля "Профессия" в форме работника - "' + action.profession.label + '"');
            return state
        case 'FORM_CHANGE_BIRTHDAY':
            console.log('Значение поля "Дата рождения" в форме работника - "'
                + moment(action.birthday, 'DD.MM.YYYY').format('DD.MM.YYYY') + '"');
            return state
        case 'FORM_CHANGE_ARCHIVE':
            console.log('Значение поля "В архиве" в форме работника - "' + action.status.label + '"');
            return state
        case 'FORM_USER_ERROR':
            console.log('При валидации формы работника возникли ошибки', action.errors);
            return state
        default:
            return state
    }
}