import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import {USERS_PROFESSIONS} from '../UserMain';
import {changePage} from '../UserActions';

class User extends Component {

    goToPage = (e,page) => {
        this.props.dispatch(changePage(page))
    }

    render() {
        let in_archive = 'Нет';
        if (this.props.user.isArchive) in_archive = 'Да';

        return (
                <Link
                      onClick={(e) => this.goToPage(e, 'Страница редактирования пользователя')}
                      to={/users/ + this.props.user.id}>
                    <div className = "border border-primary rounded p-2 mb-3 d-flex flex-row bd-highlight mb-3">
                        <div className="mr-2">
                            {this.props.num}){this.props.user.name}.
                            <div className="birthday-block ml-1">Дата рождения: {this.props.user.birthday}</div>
                        </div>
                        <div className="mr-2 d-none d-md-block">| Профессия: {USERS_PROFESSIONS[this.props.user.role]['label']}</div>
                        <div className="mr-2 d-none d-lg-block">| Номер телефона: {this.props.user.phone}</div>
                        <div className="mr-2 d-none d-xl-block">| В архиве: {in_archive}</div>
                    </div>
                </Link>

        )
    }
}

export default connect()(User);