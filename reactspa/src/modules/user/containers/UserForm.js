import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    formChangeArchive,
    formChangeBirthday,
    formChangeProfession,
    formChangeName,
    formChangePhone,
    formUserError,
    createUser,
    editUser,
    createUserError,
}
    from '../UserActions';
import {USERS_PROFESSIONS, ARCHIVE_STATUSES} from '../UserMain';
import Select from 'react-select';
import MaskedInput from 'react-maskedinput';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {Alert} from 'react-bootstrap';
import moment from 'moment';

const userFormIni = {
    id: 0,
    profession: USERS_PROFESSIONS.cook,
    birthday: new Date,
    in_archive: ARCHIVE_STATUSES.no,
    name: '',
    phone: '',
    errors: [],
    success: false
};

class UserForm extends Component {
    constructor(props) {
        super(props);

        this.state = Object.assign({}, userFormIni);
        this.state.id = this.props.usersReducer.users.length + 1;

        if (props.user) {
            this.state.id = props.user.id;
            this.state.profession = USERS_PROFESSIONS[props.user.role];
            this.state.birthday = new Date(moment(props.user.birthday, 'DD.MM.YYYY').unix());
            this.state.in_archive = (props.user.isArchive) ? ARCHIVE_STATUSES.yes : ARCHIVE_STATUSES.no;
            this.state.name = props.user.name;
            this.state.phone = props.user.phone;
        }

        this.handleFormChangeBirthday = this.handleFormChangeBirthday.bind(this);
        this.handleFormChangeProfession = this.handleFormChangeProfession.bind(this);
        this.handleFormChangeArchive = this.handleFormChangeArchive.bind(this);
        this.handleFormChangeArchive = this.handleFormChangeArchive.bind(this);
        this.handleFormChangeName = this.handleFormChangeName.bind(this);
    }

    handleFormChangeBirthday = (data) => {
        this.setState({birthday: data});
        this.props.dispatch(formChangeBirthday(data));
    }

    handleFormChangeProfession = (data) => {
        this.setState({profession: data});
        this.props.dispatch(formChangeProfession(data));

    }

    handleFormChangeArchive = (data) => {
        this.setState({in_archive: data});
        this.props.dispatch(formChangeArchive(data));

    }

    handleFormChangeName = (data) => {
        this.setState({name: data.target.value});
        this.props.dispatch(formChangeName(data.target.value));
    }


    handleFormChangePhone = (data) => {
        this.setState({phone: data.target.value});
        this.props.dispatch(formChangePhone(data.target.value));
    }

    handleFormCreateUser = (event) => {
        event.preventDefault();

        let errors = [];

        if (this.state.name == '') errors.push('Введите имя');

        var str = this.state.phone;
        if (str.indexOf('_') + 1 || str == '') errors.push('Введите корректный номер телефона');

        if (errors.length > 0) {
            this.props.dispatch(formUserError(errors));
            this.setState({errors: errors, success: false});
        } else {
            let object_result = Object.assign({}, userFormIni);

            if (this.props.user) {
                this.props.dispatch(editUser(this.state));
                object_result = this.state;
            }
            else
                this.props.dispatch(createUser(this.state));

            let cloneState = Object.assign({}, object_result);
            cloneState.success = true;
            this.setState(cloneState);
        }
    }

    render() {
        let professionsWithoutAll = [];
        for (var key in USERS_PROFESSIONS) {
            if (key != 'all') professionsWithoutAll.push({
                value: USERS_PROFESSIONS[key]['value'],
                label: USERS_PROFESSIONS[key]['label']
            })
        }

        let archiveStatuses = [];
        for (var key in ARCHIVE_STATUSES) {
            archiveStatuses.push({
                value: ARCHIVE_STATUSES[key]['value'],
                label: ARCHIVE_STATUSES[key]['label']
            });
        }

        let errors = null;
        if (this.state.errors.length > 0) errors = this.state.errors.map((error, num) =>
            <Alert key={num} variant="danger">
                {error}
            </Alert>
        )

        let title = 'Добавить сотрудника', button_name = 'Добавить';

        if (this.props.user) {
            title = 'Редактирование';
            button_name = 'Сохранить';
        }

        return (
            <div>
                <form className="mt-4 form-сreate-user" onSubmit={this.handleFormCreateUser}>
                    <b>{title}</b>

                    <div className="form-row">
                        <div className="col">
                            Имя
                            <input type="text" className="form-control" placeholder="ФИО"
                                   value={this.state.name}
                                   onChange={this.handleFormChangeName}
                            />
                        </div>

                        <div className="col">
                            Профессия
                            <div className="select-сlass">
                                <Select
                                    value={this.state.profession}
                                    options={professionsWithoutAll}
                                    onChange={this.handleFormChangeProfession}
                                />
                            </div>
                        </div>

                        <div className="col">
                            Номер телефона
                            <MaskedInput className="form-control" mask="+7 (111) 111-1111" name="phone" size="20"
                                         value={this.state.phone}
                                         onChange={this.handleFormChangePhone}
                            />
                        </div>

                        <div className="col">

                            <div>Дата рождения</div>
                            <DatePicker
                                className="form-control"
                                selected={this.state.birthday}
                                onChange={this.handleFormChangeBirthday}
                                dateFormat="DD.MM.YYYY"
                            />
                        </div>

                        <div className="col">
                            В архиве
                            <div className="select-сlass">
                                <Select
                                    value={this.state.in_archive}
                                    onChange={this.handleFormChangeArchive}
                                    options={archiveStatuses}
                                />
                            </div>
                        </div>
                    </div>

                    <input type="submit" value={button_name} className="btn btn-primary mt-3"/>
                </form>

                {errors ? <div className="mt-3">{errors}</div> : ''}

                {this.state.success ? <Alert variant="success" className = "mt-3">Пользователь добавлен успешно</Alert> : ''}
            </div>
        )
    }
}

function stateUserForm(state) {
    return {
        usersReducer: state.usersReducerContainer
    };
}

export default connect(stateUserForm)(UserForm);