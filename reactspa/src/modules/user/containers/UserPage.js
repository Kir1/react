import React, {Component} from 'react';
import {connect} from 'react-redux';
import UserForm from '../containers/UserForm';

class UserPage extends Component {
    constructor(props) {
        super(props);

        let user = null;

        for (var key in this.props.usersReducer.users) {
            if (this.props.usersReducer.users[key].id == this.props.match.params.id) {
                user = Object.assign({},this.props.usersReducer.users[key]);
                break;
            }
        }

        this.state = {
            user
        };
    }

    render() {
        return (
            <div>
                <h1>Страница пользователя</h1>

                {this.state.user ? (
                    <UserForm user={this.state.user}/>
                ) : (
                    <b>Такого пользователя не существует</b>
                )}
            </div>
        )
    }
}

function stateUserPage(state) {
    return {
        usersReducer: state.usersReducerContainer
    };
}

export default connect(stateUserPage)(UserPage);