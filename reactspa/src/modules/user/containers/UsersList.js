import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    professionFilterUsers,
    archiveStatusFilterUsers,
    sortUsers
}
    from '../UserActions';
import {USERS_PROFESSIONS, USERS_SORT_FILTERS} from '../UserMain';
import Select from 'react-select';
import User from './User';
import UserForm from '../containers/UserForm';

class UsersList extends Component {
    handleProfessionChange = (profession) => {
        this.props.dispatch(professionFilterUsers(profession));
    }

    handleArchiveStatus = (event) => {
        this.props.dispatch(archiveStatusFilterUsers(event.target.checked));
    }

    handleSort = (sort_by) => {
        this.props.dispatch(sortUsers(sort_by));
    }

    render() {
        let professions = [];
        for (var key in USERS_PROFESSIONS) {
            professions.push({
                value: USERS_PROFESSIONS[key]['value'],
                label: USERS_PROFESSIONS[key]['label']
            });
        }

        let sortFilters = [];
        for (var key in USERS_SORT_FILTERS) {
            sortFilters.push({
                value: USERS_SORT_FILTERS[key]['value'],
                label: USERS_SORT_FILTERS[key]['label']
            })
        }

        let users = null;
        if (this.props.usersReducer.filtered_users.length > 0)
            users = this.props.usersReducer.filtered_users.map((user, num) =>
                <User key={num} num={num + 1} user={user}/>
            )

        return (
            <div>
                <div>
                    <b>Профессия:</b>
                    <Select
                        value={this.props.usersReducer.profession}
                        onChange={this.handleProfessionChange}
                        options={professions}
                    />
                </div>

                <div className="mt-3">
                    <b>Сортировка по полю:</b>
                    <Select
                        value={this.props.usersReducer.sort_by}
                        onChange={this.handleSort}
                        options={sortFilters}
                    />
                </div>

                <div className="mt-3">
                    <label className="form-input-archive d-flex align-items-center">
                        <b className="mr-2">В архиве:</b>
                        <input
                            type="checkbox"
                            checked={this.props.usersReducer.in_archive}
                            onChange={this.handleArchiveStatus}/>

                    </label>
                </div>

                <UserForm/>

                <div className="mt-3">
                    {users ? (
                        <div>
                            <div className="mb-2 font-weight-bold">Сотрудники:</div>
                            {users}
                        </div>
                    ) : (
                        <b>В данный момент работников нет</b>
                    )}
                </div>
            </div>
        )
    }
}

function stateUserList(state) {
    return {
        usersReducer: state.usersReducerContainer
    };
}

export default connect(stateUserList)(UsersList);