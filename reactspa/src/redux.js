import {combineReducers, createStore} from 'redux';
import {usersReducer} from './modules/user/UserReducers';

let reducers = combineReducers({
    usersReducerContainer: usersReducer
});

export function configureStore(initialState = {}) {
    const store = createStore(reducers, initialState);
    return store;
}

export const store = configureStore();
